######## Release Build 0.0.0.5 ########

######## System Requirements ########

Hardware requirements
	> 1.6 GHz or faster processor
	> l GB of RAM (1.5 GB if running on a virtual machine)
	> 20 GB of available hard disk space
	> 5400 RPM hard disk drive
	> DirectX 9-capable video card that runs at 1024 x 768 or higher display resolution

Additional requirements
	> Microsoft windows 7 or higher
	> This version of FourZero Studio works best with Visual Studio 2013 Professional or higher. 
	> Microsoft windows .NET 4.5 framework is required

For windows CE 6.0 deve1opment
	> Visual Studio 2008 Professional is required
	> Windows CE 6.0 SDK needs to be installed available from the supplier of the hardware manufacturer
	> Additional fi1es may a1so be required, which is generally provided by the hardware manufacturer 

This Release Supports the f011owing Platforms:
	> Win32
	> winCE 6.0 (for epis hardware cosys9)

######## INSTALL INSTRUCTIONS ########
To prepare to run FourZero P1atform on your computer, execute the following steps in the order they are listed:

1. Install cmake (cmake-3.2.0-rc1-win32-x86.exe – Provided)
2. Install java runtime (jre-7u76-windows-x64.exe – Provided)
3. Install Visual Studio 2013 - Community Edition or Above (Not Provided - From Microsoft Website)
4. Copy Directory FORTE_1.7.1-distribution into your computer (Ex. C:\FORTE 1.7.1)
5. Install FourZero studio (FourzeroSetup.exe - Provided)

For cosys9 Device: Win CE 6.0 Platform
To build runtimes for epis's cosys9 device, execute the following after installing FourZero
1. Install Visual Studio 2008 Professional or Above (Not Provided)
2. Install cosys device SDK (smar9_arm9.msi – Provided)
3. Copy the following files to the "lib" folder in the win CE 6.0 SDK install folder
	a. Directory: C:\Program Files (x86)\Windows CE Tools\wce600\smart9_arm11\Lib\ARMV4I
	b. Files (comctl32.lib, winMM.lib, version.lib)
4. Copy the following files into the "include" folder in the win CE 6.0 SDK install folder
	a. DIRECTORY: C:\Program Files (x86)\Windows CE Tools\wce600\smart9_arm11\Include\ARMV4I
	b. Files (stdint.h)

This completes the setup for FourZero Platform.

######## INSTALL INSTRUCTIONS ########
You may now run the application by searching on the Start Menu or directly from C:\Program Files\Fourzero\bin\Fourzero.Launcher.exe 
