#!/bin/sh
echo "----------------------------------------------------------------------------"
echo "Sets up the LMS bin directories needed."
echo "----------------------------------------------------------------------------"
echo ""
echo "run cmake or cmake-gui to set up development environment"
echo ""
echo "run finish_lms.sh after make all finished to create forte.bin"
echo "----------------------------------------------------------------------------"
# 5/28
export forte_bin_dir="bin/lms"
export forte_ecos_built_dir=""

if [ ! -d "$forte_bin_dir" ]; then
  mkdir -p "$forte_bin_dir"
  echo "1. $forte_bin_dir allocated"
else
  echo "1. $forte_bin_dir already exists"
fi
if [ ! -d "$forte_bin_dir" ]; then
  echo "unable to create ${forte_bin_dir}"
  exit 1
fi

if [ ! -d "${forte_bin_dir}/src" ]; then
  mkdir -p "${forte_bin_dir}/src"
  echo "2. ${forte_bin_dir}/src allocated"
else
  echo "2. ${forte_bin_dir}/src already exists"
fi
if [ ! -d "${forte_bin_dir}/src" ]; then
  echo "unable to create ${forte_bin_dir}/src"
  exit 1
fi

if [ ! -d "${forte_bin_dir}/include" ]; then
  echo "3. ${forte_ecos_built_dir}/include copying...   to   $forte_bin_dir"
  cp -r ${forte_ecos_built_dir}/include $forte_bin_dir
  echo "   ${forte_ecos_built_dir}/include copied       to   $forte_bin_dir"
else
  echo "3. ${forte_ecos_built_dir}/include already exists"
fi

if [ ! -d "${forte_bin_dir}/lib" ]; then
  echo "4. ${forte_ecos_built_dir}/lib copying...   to   $forte_bin_dir"
  cp -r ${forte_ecos_built_dir}/lib $forte_bin_dir
  echo "   ${forte_ecos_built_dir}/lib copied       to   $forte_bin_dir"
else
  echo "4. ${forte_ecos_built_dir}/lib already exists"
fi
echo " LMS bin directory allocation finished."
