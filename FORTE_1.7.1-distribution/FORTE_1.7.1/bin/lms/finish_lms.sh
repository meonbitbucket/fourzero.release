#!/bin/sh
echo "----------------------------------------------------------------------------"
echo "finish_lms.sh to be run after make all finished 
echo "   to create forte.bin
echo "----------------------------------------------------------------------------"

cd "src"
arm-elf-objcopy --gap-fill 0xFF -O binary forte.elf forte.bin
echo "in: "
pwd
echo ""
ls -ls forte*
echo ""
echo " LMS bin directory allocation finished."
